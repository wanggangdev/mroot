/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import java.io.Serializable
import java.math.BigInteger

/**
 * logback日志实体类
 *
 * @author ErYang
 */
@TableName("logging_event")
class LoggingEvent : Model<LoggingEvent>(), Serializable {

    companion object {

        private const val serialVersionUID = -8552244715379304111L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         *表名
         */
        const val TABLE_NAME: String = "logging_event"

        /**
         *表前缀
         */
        const val TABLE_PREFIX: String = "logging_"

        /**
         * 日志ID
         */
        const val EVENT_ID: String = "event_id"

        /**
         * 类型
         */
        const val LEVEL_STRING: String = "level_string"

        /**
         * 文件名称
         */
        const val CALLER_FILENAME: String = "caller_filename"

        /**
         * 日志名称
         */
        const val LOGGER_NAME: String = "logger_name"

        /**
         * 线程名称
         */
        const val THREAD_NAME: String = "thread_name"

        /**
         * 类名
         */
        const val CALLER_CLASS: String = "caller_class"

        /**
         * 方法
         */
        const val CALLER_METHOD: String = "caller_method"

        /**
         * 参数
         */
        const val ARG0: String = "arg0"

        /**
         * 参数
         */
        const val ARG1: String = "arg1"

        /**
         * 参数
         */
        const val ARG2: String = "arg2"

        /**
         * 参数
         */
        const val ARG3: String = "arg3"

        /**
         * 日志信息
         */
        const val FORMATTED_MESSAGE: String = "formatted_message"

        /**
         * 引用
         */
        const val REFERENCE_FLAG: String = "reference_flag"

        /**
         * 代码行
         */
        const val CALLER_LINE: String = "caller_line"

        /**
         * 创建时间
         */
        const val TIMESTMP: String = "timestmp"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param event  Event
         * @return Event
         */
        fun copy2New(event: LoggingEvent): LoggingEvent {
            val newEvent = LoggingEvent()
            newEvent.eventId = event.eventId
            newEvent.levelString = event.levelString
            newEvent.callerFilename = event.callerFilename
            newEvent.loggerName = event.loggerName
            newEvent.threadName = event.threadName
            newEvent.callerClass = event.callerClass
            newEvent.callerMethod = event.callerMethod
            newEvent.arg0 = event.arg0
            newEvent.arg1 = event.arg1
            newEvent.arg2 = event.arg2
            newEvent.arg3 = event.arg3
            newEvent.formattedMessage = event.formattedMessage
            newEvent.referenceFlag = event.referenceFlag
            newEvent.callerLine = event.callerLine
            newEvent.timestmp = event.timestmp
            return newEvent
        }

        // -------------------------------------------------------------------------------------------------

    }

    /**
     * ID
     */
    @TableId(value = "event_id", type = IdType.AUTO)
    var eventId: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    var levelString: String? = null

    // -------------------------------------------------------------------------------------------------


    var callerFilename: String? = null

    // -------------------------------------------------------------------------------------------------


    var loggerName: String? = null

    // -------------------------------------------------------------------------------------------------


    var threadName: String? = null

    // -------------------------------------------------------------------------------------------------


    var callerClass: String? = null

    // -------------------------------------------------------------------------------------------------


    var callerMethod: String? = null

    // -------------------------------------------------------------------------------------------------


    var arg0: String? = null

    // -------------------------------------------------------------------------------------------------


    var arg1: String? = null

    // -------------------------------------------------------------------------------------------------


    var arg2: String? = null

    // -------------------------------------------------------------------------------------------------


    var arg3: String? = null

    // -------------------------------------------------------------------------------------------------


    var formattedMessage: String? = null

    // -------------------------------------------------------------------------------------------------


    var referenceFlag: String? = null

    // -------------------------------------------------------------------------------------------------


    var callerLine: String? = null

    // -------------------------------------------------------------------------------------------------


    var timestmp: BigInteger? = null

    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.eventId
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LoggingEvent

        if (eventId != other.eventId) return false
        if (levelString != other.levelString) return false
        if (callerFilename != other.callerFilename) return false
        if (loggerName != other.loggerName) return false
        if (threadName != other.threadName) return false
        if (callerClass != other.callerClass) return false
        if (callerMethod != other.callerMethod) return false
        if (arg0 != other.arg0) return false
        if (arg1 != other.arg1) return false
        if (arg2 != other.arg2) return false
        if (arg3 != other.arg3) return false
        if (formattedMessage != other.formattedMessage) return false
        if (referenceFlag != other.referenceFlag) return false
        if (callerLine != other.callerLine) return false
        if (timestmp != other.timestmp) return false

        return true
    }

    override fun hashCode(): Int {
        var result = eventId?.hashCode() ?: 0
        result = 31 * result + (levelString?.hashCode() ?: 0)
        result = 31 * result + (callerFilename?.hashCode() ?: 0)
        result = 31 * result + (loggerName?.hashCode() ?: 0)
        result = 31 * result + (threadName?.hashCode() ?: 0)
        result = 31 * result + (callerClass?.hashCode() ?: 0)
        result = 31 * result + (callerMethod?.hashCode() ?: 0)
        result = 31 * result + (arg0?.hashCode() ?: 0)
        result = 31 * result + (arg1?.hashCode() ?: 0)
        result = 31 * result + (arg2?.hashCode() ?: 0)
        result = 31 * result + (arg3?.hashCode() ?: 0)
        result = 31 * result + (formattedMessage?.hashCode() ?: 0)
        result = 31 * result + (referenceFlag?.hashCode() ?: 0)
        result = 31 * result + (callerLine?.hashCode() ?: 0)
        result = 31 * result + (timestmp?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "LoggingEvent(eventId=$eventId, levelString=$levelString, callerFilename=$callerFilename, loggerName=$loggerName, threadName=$threadName, callerClass=$callerClass, callerMethod=$callerMethod, arg0=$arg0, arg1=$arg1, arg2=$arg2, arg3=$arg3, formattedMessage=$formattedMessage, referenceFlag=$referenceFlag, callerLine=$callerLine, timestmp=$timestmp)"
    }


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LoggingEvent class

/* End of file LoggingEvent.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/LoggingEvent.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
