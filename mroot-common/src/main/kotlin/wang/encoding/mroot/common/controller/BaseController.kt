/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.common.controller

import org.springframework.beans.factory.annotation.Autowired
import wang.encoding.mroot.common.util.HttpRequestUtil
import wang.encoding.mroot.common.config.LocaleMessageSourceConfiguration
import javax.servlet.http.HttpServletRequest


/**
 * 基类控制器
 *
 * @author ErYang
 */
open class BaseController {

    @Autowired
    protected lateinit var localeMessageSourceConfiguration: LocaleMessageSourceConfiguration

    @Autowired
    private lateinit var httpRequestUtil: HttpRequestUtil

    companion object {

        /**
         * 默认视图目录
         */
        private const val DEFAULT_VIEW = "/default"

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 可读可写
     */
    //@Autowired
    //private lateinit var request: HttpServletRequest

    /**
     * 只读
     */
    //@Autowired
    //private val request: HttpServletRequest? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否ajax请求
     *
     * @return Boolean
     */
    protected fun isAjaxRequest(request: HttpServletRequest): Boolean {
        return httpRequestUtil.isAjaxRequest(request)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到用户的 ip
     *
     * @param request HttpServletRequest
     * @return String ip地址
     */
    fun getIp(request: HttpServletRequest?): String {
        return httpRequestUtil.getIp(request)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseController class

/* End of file BaseController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/controller/BaseController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
