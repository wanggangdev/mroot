/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config


import com.alibaba.druid.pool.DruidDataSource
import com.alibaba.druid.support.http.WebStatFilter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

import javax.sql.DataSource
import java.sql.SQLException

/**
 * Druid配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class DruidConfiguration {

    companion object {

        private val logger: Logger = LoggerFactory.getLogger(DruidConfiguration::class.java)
    }

    // -------------------------------------------------------------------------------------------------

    @Value("\${spring.datasource.url}")
    private val dbUrl: String? = null

    @Value("\${spring.datasource.username}")
    private val username: String? = null

    @Value("\${spring.datasource.password}")
    private val password: String? = null

    @Value("\${druid.datasource.driverClassName}")
    private val driverClassName: String? = null

    @Value("\${druid.datasource.initialSize}")
    private val initialSize: Int = 0

    @Value("\${druid.datasource.minIdle}")
    private val minIdle: Int = 0

    @Value("\${druid.datasource.maxActive}")
    private val maxActive: Int = 0

    @Value("\${druid.datasource.maxWait}")
    private val maxWait: Int = 0

    @Value("\${druid.datasource.timeBetweenEvictionRunsMillis}")
    private val timeBetweenEvictionRunsMillis: Int = 0

    @Value("\${druid.datasource.minEvictableIdleTimeMillis}")
    private val minEvictableIdleTimeMillis: Int = 0

    @Value("\${druid.datasource.validationQuery}")
    private val validationQuery: String? = null

    @Value("\${druid.datasource.testWhileIdle}")
    private val testWhileIdle: Boolean = false

    @Value("\${druid.datasource.testOnBorrow}")
    private val testOnBorrow: Boolean = false

    @Value("\${druid.datasource.testOnReturn}")
    private val testOnReturn: Boolean = false

    @Value("\${druid.datasource.poolPreparedStatements}")
    private val poolPreparedStatements: Boolean = false

    @Value("\${druid.datasource.maxPoolPreparedStatementPerConnectionSize}")
    private val maxPoolPreparedStatementPerConnectionSize: Int = 0

    @Value("\${druid.datasource.filters}")
    private val filters: String? = null

    @Value("{druid.datasource.connectionProperties}")
    private val connectionProperties: String? = null

    /**
     * 声明其为Bean实例
     *
     * @return DataSource
     */
    @Bean("dataSource")
    @Primary
    fun dataSource(): DataSource {
        val datasource = DruidDataSource()
        // 配置
        datasource.url = this.dbUrl
        datasource.username = username
        datasource.password = password
        datasource.driverClassName = driverClassName
        datasource.initialSize = initialSize
        datasource.minIdle = minIdle
        datasource.maxActive = maxActive
        datasource.maxWait = maxWait.toLong()
        datasource.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis.toLong()
        datasource.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis.toLong()
        datasource.validationQuery = validationQuery
        datasource.isTestWhileIdle = testWhileIdle
        datasource.isTestOnBorrow = testOnBorrow
        datasource.isTestOnReturn = testOnReturn
        datasource.isPoolPreparedStatements = poolPreparedStatements
        datasource.maxPoolPreparedStatementPerConnectionSize = maxPoolPreparedStatementPerConnectionSize
        try {
            datasource.setFilters(filters)
        } catch (e: SQLException) {
            if (logger.isErrorEnabled) {
                logger.error(">>>>>>>>Druid设置过滤器错误<<<<<<<<", e)
            }
        }
        datasource.setConnectionProperties(connectionProperties)
        return datasource
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 注册一个 filterRegistrationBean
     *
     * @return FilterRegistrationBean
     */
    @Bean
    fun druidStatFilter(): FilterRegistrationBean<*> {
        val filterRegistrationBean: FilterRegistrationBean<WebStatFilter> = FilterRegistrationBean(WebStatFilter())
        // 添加过滤规则
        filterRegistrationBean.addUrlPatterns("/*")
        // 添加不需要忽略的格式信息
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*")
        return filterRegistrationBean
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DruidConfiguration class

/* End of file DruidConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/DruidConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
