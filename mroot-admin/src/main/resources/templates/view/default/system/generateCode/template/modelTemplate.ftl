/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${modelPackageName}.${classPrefix}


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern
import java.io.Serializable


/**
 * ${classComment}实体类
 *
 * @author ErYang
 */
@TableName("${tableName}")
class ${className} : Model<${className}>(), Serializable {

 companion object {

    private const val serialVersionUID = -1L

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
    */
   const val TABLE_NAME : String= "${tableName}"

    /**
     *表前缀
    */
    const val TABLE_PREFIX: String = "${tablePrefix}_"

<#list generateModels as var>
    /**
    * ${var.comment}
    */
    const val ${var.finalName} : String= "${var.name}"

</#list>
    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param ${classFirstLowerCaseName}  ${className}
         * @return ${className}
         */
        fun copy2New(${classFirstLowerCaseName}: ${className}): ${className} {
            val new${className} = ${className}()
<#list generateModels as var>
    new${className}.${var.camelCaseName}=${classFirstLowerCaseName}.${var.camelCaseName}
</#list>
            return new${className}
        }

        // -------------------------------------------------------------------------------------------------

    }

<#list generateModels as var>

    <#if "type" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 10, message = "validation.type.range")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "name" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.name.pattern")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "title" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "value" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9_{}\":,]{2,255}$", message = "validation.value.pattern")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "status" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "gmtCreate" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Past(message = "validation.gmtCreate.past")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "gmtCreateIp" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "gmtModified" == var.camelCaseName >
   /**
    * ${var.comment}
    */
     @Past(message = "validation.gmtModified.past")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "sort" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Range(min = 0, message = "validation.sort.range")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "remark" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    <#if "status" != var.camelCaseName && "title" != var.camelCaseName
    && "name" != var.camelCaseName && "type" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sort" != var.camelCaseName && "remark" != var.camelCaseName>
        <#if dataId == var.name>
    /**
    * ${var.comment}
    */
    @TableId(value = "${var.name}", type = IdType.AUTO)
        </#if>
      /**
    * ${var.comment}
    */
    var ${var.camelCaseName}: ${var.type}?= null
    </#if>

    // -------------------------------------------------------------------------------------------------
</#list>

 override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ${className} class

/* End of file ${className}.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/${classPrefix}/${className}.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
