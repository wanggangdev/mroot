/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package wang.encoding.mroot.admin.controller.${classPrefix}.${classFirstLowerCaseName}



import com.alibaba.fastjson.JSONObject
import com.baomidou.mybatisplus.plugins.Page
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import ${modelPackageName}.${classPrefix}.${className}
import wang.encoding.mroot.model.enums.StatusEnum
import ${servicePackageName}.${classPrefix}.${className}Service
import java.math.BigInteger
import javax.servlet.http.HttpServletRequest
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.business.ResultData


/**
* 后台 ${classComment} 控制器
*
*@author ErYang
*/
@RestController
@RequestMapping(value = ["/${classFirstLowerCaseName}"])
class ${className}Controller : BaseAdminController() {


	    @Autowired
    private lateinit var ${classFirstLowerCaseName}Service: ${className}Service


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(${className}Controller::class.java)

        /**
         * 模块
         */
        private const val MODULE_NAME: String  = "/${classFirstLowerCaseName}"
        /**
         * 视图目录
         */
        private const val VIEW_PATH: String = "/${classPrefix}/${classFirstLowerCaseName}"
          /**
         * 对象名称
         */
        private const val VIEW_MODEL_NAME: String  = "${classFirstLowerCaseName}"
        /**
         * 首页
         */
        private const val INDEX: String  = "/index"
        private const val INDEX_URL: String = MODULE_NAME + INDEX
        private const val INDEX_VIEW: String = VIEW_PATH + INDEX
        /**
         * 新增
         */
        private const val ADD: String  = "/add"
        private const val ADD_URL: String = MODULE_NAME + ADD
        private const val ADD_VIEW: String = VIEW_PATH + ADD
        /**
         * 保存
         */
        private const val SAVE: String  = "/save"
        private const val SAVE_URL: String = MODULE_NAME + SAVE
        /**
         * 修改
         */
        private const val EDIT: String  = "/edit"
        private const val EDIT_URL: String = MODULE_NAME + EDIT
        private const val EDIT_VIEW: String = VIEW_PATH + EDIT
        /**
         * 更新
         */
        private const val UPDATE: String  = "/update"
        private const val UPDATE_URL: String = MODULE_NAME + UPDATE
        /**
         * 查看
         */
        private const val VIEW: String  = "/view"
        private const val VIEW_URL: String = MODULE_NAME + VIEW
        private const val VIEW_VIEW: String = VIEW_PATH + VIEW
        /**
         * 删除
         */
        private const val DELETE: String  = "/delete"
        private const val DELETE_URL: String = MODULE_NAME + DELETE
        private const val DELETE_BATCH: String  = "/deleteBatch"
        private const val DELETE_BATCH_URL: String = MODULE_NAME + DELETE_BATCH

        /**
         * 回收站
         */
        private const val RECYCLE_BIN_INDEX: String = "/recycleBin"
        private const val RECYCLE_BIN_INDEX_URL: String = MODULE_NAME + RECYCLE_BIN_INDEX
        private const val RECYCLE_BIN_INDEX_VIEW: String = VIEW_PATH + RECYCLE_BIN_INDEX
        /**
         * 恢复
         */
        private const val RECOVER: String = "/recover"
        private const val RECOVER_URL: String = MODULE_NAME + RECOVER

        private const val RECOVER_BATCH: String = "/recoverBatch"
        private const val RECOVER_BATCH_URL: String = MODULE_NAME + RECOVER_BATCH

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_INDEX)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val ${classFirstLowerCaseName} = ${className}()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
${classFirstLowerCaseName}.title = title
            modelAndView.addObject("title", title)
        }
${classFirstLowerCaseName}.status = StatusEnum.NORMAL.key
        val page: Page<${className}> = ${classFirstLowerCaseName}Service.list2page(super.initPage(request), ${classFirstLowerCaseName}, ${className}.ID, false)!!
    modelAndView.addObject(VIEW_PAGE_NAME, page)
    return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [ADD_URL])
    @RequestMapping(ADD)
     @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_ADD)
    @FormToken(init = true)
    fun add(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(ADD_VIEW))
        super.initViewTitleAndModelUrl(ADD_URL, MODULE_NAME, request)

 // 最大排序值
        val maxSort: Int = ${classFirstLowerCaseName}Service.getMax2Sort() + 1
        modelAndView.addObject("maxSort", maxSort)

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param request HttpServletRequest
     * @param ${classFirstLowerCaseName} ${className}
     * @return Any
     */
    @RequiresPermissions(value = [SAVE_URL])
    @RequestMapping(SAVE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_SAVE)
    @FormToken(remove = true)
    fun save(request: HttpServletRequest, ${classFirstLowerCaseName}: ${className}): Any {

  if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()

            // 创建 ${className} 对象
            val save${className}: ${className} = this.initAddData(request, ${classFirstLowerCaseName})

            // Hibernate Validation  验证数据
            val validationResult: String? = ${classFirstLowerCaseName}Service.validation${className}(save${className})
            if (null != validationResult && validationResult.isNotBlank()) {
            return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationAddData(save${className}, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }


            // 新增用户 id
            val id: BigInteger? = ${classFirstLowerCaseName}Service.saveBackId(save${className})
            //if (null != id && BigInteger.ZERO < id) {
                // //异步清理缓存
                //this.removeCacheEvent(id)
            //}
            return super.initSaveJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
        }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
   @RequiresPermissions(value = [EDIT_URL])
    @RequestMapping("$EDIT/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_EDIT)
    @FormToken(init = true)
    fun edit(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(EDIT_VIEW))


        super.initViewTitleAndModelUrl(EDIT_URL, MODULE_NAME, request)
        val idValue: BigInteger? = super.getId(id)
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val ${classFirstLowerCaseName}: ${className}? = ${classFirstLowerCaseName}Service.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (StatusEnum.DELETE.key == ${classFirstLowerCaseName}!!.status) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        modelAndView.addObject(VIEW_MODEL_NAME, ${classFirstLowerCaseName})
        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param request HttpServletRequest
     * @param ${classFirstLowerCaseName} ${className}
     * @return Any
     */
      @RequiresPermissions(value = [UPDATE_URL])
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_UPDATE)
    @FormToken(remove = true)
    fun update(request: HttpServletRequest,  ${classFirstLowerCaseName}: ${className}): Any {
     if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()
            // 验证数据
            val idValue: BigInteger? = super.getId(request)
            if (null == idValue || BigInteger.ZERO > idValue) {
              return super.initErrorCheckJSONObject(failResult)
            }
            // 数据真实性
            val ${classFirstLowerCaseName}Before: ${className}? = ${classFirstLowerCaseName}Service.getById(idValue)
            if(null == ${classFirstLowerCaseName}Before){
               return super.initErrorCheckJSONObject(failResult)
            }
            if (StatusEnum.DELETE.key == ${classFirstLowerCaseName}Before.status) {
              return super.initErrorCheckJSONObject(failResult)
            }

            // 创建 ${className} 对象
            var edit${className}: ${className} = ${className}.copy2New(${classFirstLowerCaseName})
            edit${className}.id = ${classFirstLowerCaseName}Before.id
            val statusStr: String? = super.getStatusStr(request)
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) -> edit${className}.status = StatusEnum.NORMAL.key
                null == statusStr -> edit${className}.status = StatusEnum.DISABLE.key
                else -> edit${className}.status = StatusEnum.DISABLE.key
            }
            edit${className} = this.initEditData(edit${className})

            // Hibernate Validation 验证数据
            val validationResult: String? = ${classFirstLowerCaseName}Service.validation${className}(edit${className})
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationEditData(${classFirstLowerCaseName},
${classFirstLowerCaseName}Before, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 修改 ${className} id
            val id: BigInteger? = ${classFirstLowerCaseName}Service.updateBackId(edit${className})
            //if (null != id && BigInteger.ZERO < id) {
                // //异步清理缓存
                //this.removeCacheEvent(id)
            //}
            return super.initUpdateJSONObject(id)
        }else{
            return super.initErrorRedirectUrl()
        }
}

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [VIEW_URL])
    @RequestMapping("$VIEW/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_VIEW)
    fun view(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(VIEW_VIEW))
        super.initViewTitleAndModelUrl(VIEW_URL, MODULE_NAME, request)

        val idValue: BigInteger? = super.getId(id)
        // 验证数据
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
 // 数据真实性
        val ${classFirstLowerCaseName}: ${className}? = ${classFirstLowerCaseName}Service.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
       if(null!=${classFirstLowerCaseName}){
modelAndView.addObject(VIEW_MODEL_NAME, ${classFirstLowerCaseName})
}

 // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
      @RequiresPermissions(value = [DELETE_URL])
    @RequestMapping("$DELETE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_DELETE)
    fun delete(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val ${classFirstLowerCaseName}: ${className}? = ${classFirstLowerCaseName}Service.getById(idValue!!)
            if (null == ${classFirstLowerCaseName}) {
                super.initErrorCheckJSONObject()
            }
            // 删除 ${className} id
            val backId: BigInteger? = ${classFirstLowerCaseName}Service.removeBackId(${classFirstLowerCaseName}!!.id!!)
   //if (null != backId && BigInteger.ZERO < backId) {
        //    // 异步清理缓存
            //this.removeCacheEvent(backId)
        //}
            return super.initDeleteJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     * @param request HttpServletRequest
     * @return JSONObject
     */
     @RequiresPermissions(value = [DELETE_BATCH_URL])
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_DELETE_BATCH)
    fun deleteBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList
<BigInteger>? = super.getIdArray(request)
    // 验证数据
    if (null == idArray || idArray.isEmpty()) {
    super.initErrorCheckJSONObject()
    }
    var flag = false
    if (null != idArray) {
    flag = ${classFirstLowerCaseName}Service.removeBatch2UpdateStatus(idArray)!!
    }
    return if (flag) {

    // // 异步清理缓存
    //this.removeBatchCacheEvent(idArray!!)
    super.initDeleteJSONObject(BigInteger.ONE)
    } else {
    super.initDeleteJSONObject(BigInteger.ZERO)
    }
    }
    return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 回收站页面
    *
    * @param request HttpServletRequest
    * @return ModelAndView
    */
    @RequiresPermissions(value = [RECYCLE_BIN_INDEX_URL])
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
    title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECYCLE_BIN_INDEX)
    fun recycleBin(request: HttpServletRequest): ModelAndView {
    val modelAndView = ModelAndView(super.initView(RECYCLE_BIN_INDEX_VIEW))
    super.initViewTitleAndModelUrl(RECYCLE_BIN_INDEX_URL, MODULE_NAME, request)

    val ${classFirstLowerCaseName} = ${className}()
    val title: String? = request.getParameter("title")
    if (null != title && title.isNotBlank()) {
${classFirstLowerCaseName}.title = title
    modelAndView.addObject("title", title)
    }
${classFirstLowerCaseName}.status = StatusEnum.DELETE.key
    val page: Page<${className}> = ${classFirstLowerCaseName}Service.list2page(super.initPage(request), ${classFirstLowerCaseName}, ${className}.ID, false)!!
    modelAndView.addObject(VIEW_PAGE_NAME, page)
    return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 恢复
    * @param request HttpServletRequest
    * @param id String
    * @return JSONObject
    */
    @RequiresPermissions(value = [RECOVER_URL])
    @RequestMapping("$RECOVER/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
    title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECOVER)
    fun recover(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
    if (super.isAjaxRequest(request)) {
    val idValue: BigInteger? = super.getId(id)
    // 验证数据
    if (null == idValue || BigInteger.ZERO > idValue) {
    super.initErrorCheckJSONObject()
    }
    val ${classFirstLowerCaseName}: ${className}? = ${classFirstLowerCaseName}Service.getById(idValue!!)
    if (null == ${classFirstLowerCaseName}) {
    super.initErrorCheckJSONObject()
    }
    // 恢复 ${className} id
    val backId: BigInteger? = ${classFirstLowerCaseName}Service.recoverBackId(${classFirstLowerCaseName}!!.id!!)
    //if (null != backId && BigInteger.ZERO < backId) {
    // //异步清理缓存
    //this.removeCacheEvent(backId)
    // }
    return super.initRecoverJSONObject(backId)
    }
    return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 批量恢复
    * @param request HttpServletRequest
    * @return JSONObject
    */
    @RequiresPermissions(value = [RECOVER_BATCH_URL])
    @RequestMapping(RECOVER_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
    title = RequestLogConstant.ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECOVER_BATCH)
    fun recoverBatch(request: HttpServletRequest): JSONObject? {
    if (super.isAjaxRequest(request)) {
    val idArray: ArrayList
    <BigInteger>? = super.getIdArray(request)
        // 验证数据
        if (null == idArray || idArray.isEmpty()) {
        super.initErrorCheckJSONObject()
        }
        var flag = false
        if (null != idArray) {
        flag = ${classFirstLowerCaseName}Service.recoverBatch2UpdateStatus(idArray)!!
        }
        return if (flag) {
        // 异步清理缓存
        //this.removeBatchCacheEvent(idArray!!)
        super.initRecoverJSONObject(BigInteger.ONE)
        } else {
        super.initRecoverJSONObject(BigInteger.ZERO)
        }
        }
        return super.initReturnErrorJSONObject()
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 合法性
        *
        * @param request HttpServletRequest
        * @param ${classFirstLowerCaseName}         ${className}
        * @return ${className}
        */
        private fun initAddData(request: HttpServletRequest, ${classFirstLowerCaseName}: ${className}): ${className} {
        val add${className}: ${className} = ${className}.copy2New(${classFirstLowerCaseName})
        // ip
        add${className}.gmtCreateIp = super.getIp(request)
        // 创建 ${className} 对象
        return ${classFirstLowerCaseName}Service.initSave${className}(add${className})
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 合法性
        *
        * @param ${classFirstLowerCaseName}         ${className}
        * @return ${className}
        */
        private fun initEditData(${classFirstLowerCaseName}: ${className}): ${className} {
        val edit${className}: ${className} = ${classFirstLowerCaseName}
        // 创建 ${className} 对象
        return ${classFirstLowerCaseName}Service.initEdit${className}(edit${className})
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 数据的唯一性
        *
        * @param ${classFirstLowerCaseName}         ${className}
        * @param failResult ResultData
        * @return Boolean true(通过)/false(未通过)
        */
        private fun validationAddData(${classFirstLowerCaseName}: ${className},
        failResult: ResultData):
        Boolean {
        val message: String
        // 是否存在
        val nameExist: ${className}? = ${classFirstLowerCaseName}Service.getByName(${classFirstLowerCaseName}
        .name!!.trim().toLowerCase())
        if (null != nameExist) {

        message = localeMessageSourceConfiguration.getMessage("message.name.exist")
        failResult.setFail()[configProperties.messageName] = message
        return false
        }

        return true
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 数据的唯一性
        *
        * @param ${classFirstLowerCaseName}         ${className}
        * @param failResult ResultData
        * @return Boolean true(通过)/false(未通过)
        */
        private fun validationEditData(new${className}: ${className}, ${classFirstLowerCaseName}: ${className},
        failResult: ResultData): Boolean {
        val message: String
        // 是否存在
        val nameExist: Boolean = ${classFirstLowerCaseName}Service.propertyUnique(${className}.NAME, new${className}
        .name!!.trim().toLowerCase(), ${classFirstLowerCaseName}.name!!.toLowerCase())
        if (!nameExist) {
        message = localeMessageSourceConfiguration.getMessage("message.name.exist")
        failResult.setFail()[configProperties.messageName] = message
        return false
        }

        val titleExist: Boolean = ${classFirstLowerCaseName}Service.propertyUnique(${className}.TITLE, new${className}
        .title!!.trim().toLowerCase(), ${classFirstLowerCaseName}.title!!.toLowerCase())
        if (!titleExist) {
        message = localeMessageSourceConfiguration.getMessage("message.title.exist")
        failResult.setFail()[configProperties.messageName] = message
        return false
        }
        return true
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 根据 ID 清理缓存
        *
        * @param id         BigInteger
        *
        */
        private fun removeCacheEvent(id: BigInteger) {
        // 异步清理缓存
        val remove${className}CacheEvent = Remove${className}CacheEvent(this, id)
        super.applicationContext.publishEvent(remove${className}CacheEvent)
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 根据 ID 集合批量清理缓存
        *
        * @param idArray         ArrayList<BigInteger>
            *
            */
            private fun removeBatchCacheEvent(idArray: ArrayList<BigInteger>) {
            // 异步清理缓存
            val remove${className}CacheEvent = Remove${className}CacheEvent(this, idArray)
            super.applicationContext.publishEvent(remove${className}CacheEvent)
            }

            // -------------------------------------------------------------------------------------------------


            /**
        * ${classComment}
        */
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_INDEX: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.index"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_ADD: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.add"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_SAVE: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.save"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_EDIT: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.edit"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_UPDATE: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.update"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_VIEW: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.view"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_DELETE: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.delete"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_DELETE_BATCH: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.delete.batch"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECYCLE_BIN_INDEX: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.recycleBin"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECOVER: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.recover"
        const val  ADMIN_MODULE_${classPrefixUppercase}_${classNameUppercase}_RECOVER_BATCH: String =
        "message.aop.requestLog.module.admin.${classPrefix}.${classFirstLowerCaseName}.recover.batch"

        }

        // -----------------------------------------------------------------------------------------------------

        // End ${className}Controller class

        /* End of file ${className}Controller.kt */
        /* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/${classPrefix}/${classFirstLowerCaseName}/${className}Controller.kt */

        // -----------------------------------------------------------------------------------------------------
        // +----------------------------------------------------------------------------------------------------
        // | ErYang出品 属于小极品 O(∩_∩)O~~ 共同学习 共同进步
        // +----------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------
