/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${servicePackageName}.${classPrefix}.impl


import org.springframework.beans.factory.annotation.Autowired
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import ${modelPackageName}.${classPrefix}.${className}
import wang.encoding.mroot.model.enums.StatusEnum
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.${classPrefix}.${className}Mapper
import wang.encoding.mroot.service.${classPrefix}.${className}Service

import java.math.BigInteger
import java.time.Instant
import java.util.Date


/**
 * 后台 ${classComment} Service 接口实现类
*
* @author ErYang
 */
@Service
class ${className}ServiceImpl : BaseServiceImpl
<${className}Mapper, ${className}>(), ${className}Service {

   companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(${className}ServiceImpl::class.java)
    }

    /**
     * 初始化新增 ${className} 对象
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ${className}
     */
    override fun  initSave${className}(${classFirstLowerCaseName}:${className}):${className} {
${classFirstLowerCaseName}.status=StatusEnum.NORMAL.key
${classFirstLowerCaseName}.gmtCreate=Date.from(Instant.now())
        return ${classFirstLowerCaseName}
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 ${className} 对象
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ${className}
     */
    override fun initEdit${className}( ${classFirstLowerCaseName}:${className}):${className}{
${classFirstLowerCaseName}.gmtModified = Date.from(Instant.now())
        return ${classFirstLowerCaseName}
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun  validation${className}(${classFirstLowerCaseName}:${className}):String? {
        return HibernateValidationUtil.validateEntity(${classFirstLowerCaseName})
    }

    // -------------------------------------------------------------------------------------------------

   /**
     * 根据 id 查询 ${className} 缓存
     *
     * @param id ID
     * @return ${className}
     */
    @Cacheable(key = "#id")
    override fun getById2Cache(id: BigInteger): ${className}? {
        return super.getById(id)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 ${classFirstLowerCaseName}
     *
     * @param name 标识
     * @return ${className}
     */
    override fun getByName(name: String): ${className}? {
        val ${classFirstLowerCaseName} = ${className}()
${classFirstLowerCaseName}.name = name
        return super.getByModel(${classFirstLowerCaseName})
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 ${classFirstLowerCaseName}
     *
     * @param title 名称
     * @return ${className}
     */
    override fun getByTitle(title: String): ${className}? {
        val ${classFirstLowerCaseName} = ${className}()
${classFirstLowerCaseName}.title = title
        return super.getByModel(${classFirstLowerCaseName})
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 ${classComment}
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ID  BigInteger
     */
    override fun  saveBackId(${classFirstLowerCaseName}:${className}):BigInteger?{
        val id:Int?= super.save(${classFirstLowerCaseName})
        if (null != id && 0 < id) {
            return ${classFirstLowerCaseName}.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 ${classComment}(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun  removeBackId(id:BigInteger):BigInteger?{
val ${classFirstLowerCaseName} =  ${className}()
${classFirstLowerCaseName}.id=id
${classFirstLowerCaseName}.status=StatusEnum.DELETE.key
${classFirstLowerCaseName}.gmtModified= Date.from(Instant.now())
        val backId:Int? = super.remove2StatusById(${classFirstLowerCaseName})
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val ${classFirstLowerCaseName} = ${className}()
${classFirstLowerCaseName}.id = id
${classFirstLowerCaseName}.status = StatusEnum.NORMAL.key
${classFirstLowerCaseName}.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(${classFirstLowerCaseName})
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 ${classComment}
     *
     * @param ${classFirstLowerCaseName} ${className}
     * @return ID  BigInteger
     */
    override fun  updateBackId(${classFirstLowerCaseName}:${className}):BigInteger? {
        val flag:Boolean = super.updateById(${classFirstLowerCaseName})
        if (flag) {
            return ${classFirstLowerCaseName}.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ${className}ServiceImpl class

/* End of file ${className}ServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/${classPrefix}/impl/${className}ServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
