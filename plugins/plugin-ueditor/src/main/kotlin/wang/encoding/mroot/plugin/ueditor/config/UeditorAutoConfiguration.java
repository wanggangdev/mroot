/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import wang.encoding.mroot.plugin.ueditor.ActionEnter;
import wang.encoding.mroot.plugin.ueditor.ConfigManager;
import wang.encoding.mroot.plugin.ueditor.hunter.FileManager;
import wang.encoding.mroot.plugin.ueditor.upload.StorageManager;


/**
 * 配置
 *
 * @author ErYang
 */
@Configuration
@EnableConfigurationProperties(UeditorProperties.class)
@ConditionalOnClass(ActionEnter.class)
@ConditionalOnProperty(prefix = "ueditor", value = "enabled", matchIfMissing = true)
public class UeditorAutoConfiguration {

    private final UeditorProperties ueditorProperties;

    @Autowired
    public UeditorAutoConfiguration(UeditorProperties ueditorProperties) {
        this.ueditorProperties = ueditorProperties;
    }

    /**
     * 注册 bean
     *
     * @return ActionEnter
     */
    @Bean
    @ConditionalOnMissingBean(ActionEnter.class)
    public ActionEnter actionEnter() {
        return new ActionEnter(new ConfigManager(ueditorProperties.getConfig()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 注入配置
     */
    @PostConstruct
    public void storageManager() {
        StorageManager.accessKey = FileManager.accessKey = ueditorProperties.getAccessKey();
        StorageManager.secretKey = FileManager.secretKey = ueditorProperties.getSecretKey();
        StorageManager.baseUrl = FileManager.baseUrl = ueditorProperties.getBaseUrl();
        StorageManager.bucket = FileManager.bucket = ueditorProperties.getBucket();
        StorageManager.baseUrl = FileManager.baseUrl = ueditorProperties.getBaseUrl();
        StorageManager.uploadDirPrefix = FileManager.uploadDirPrefix = ueditorProperties.getUploadDirPrefix();
        StorageManager.zone = FileManager.zone = ueditorProperties.getZoneObj();
        StorageManager.uploadLocal = FileManager.uploadLocal = ueditorProperties.getUploadLocal();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UeditorAutoConfiguration class

/* End of file UeditorAutoConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/plugin/ueditor/config/UeditorAutoConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
